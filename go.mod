module bitbucket.org/wikivr/api-scraper

go 1.12

require (
	cgt.name/pkg/go-mwclient v1.0.3
	github.com/antonholmquist/jason v1.0.0 // indirect
	github.com/golang/protobuf v1.3.1
	github.com/mrjones/oauth v0.0.0-20190623134757-126b35219450 // indirect
	github.com/sirupsen/logrus v1.4.2
	google.golang.org/grpc v1.22.0
)
