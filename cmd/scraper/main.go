package main

import (
    "flag"
    "log"

    "bitbucket.org/wikivr/api-scraper/pkg"
)

var (
    scraperAddr = flag.String("scrapper-addr", "localhost", "The address of the scrapper server")
    scraperPort = flag.Int("scrapper-port", 6592, "The port number of the scrapper server")
    comprehensionAddr = flag.String("comprehension-addr", "localhost", "The address of the comprehension server")
    comprehensionPort = flag.Int("comprehension-port", 4592, "The port number of the comprehension server")
)
func main() {

    flag.Parse()

    cfg := pkg.Config{
        ScraperAddr: *scraperAddr,
        ScraperPort: *scraperPort,
        ComprehensionAddr: *comprehensionAddr,
        ComprehensionPort: *comprehensionPort,
    }

    if err := pkg.Start(cfg); err != nil {
        log.Fatalf("Failed to start the server. Error: %+v", err)
    }
}
