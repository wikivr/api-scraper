#!/bin/bash

export GO111MODULE=on

# generate the protobuf go code
protoc --go_out=plugins=grpc:pkg/ proto/gen/comprehension.proto

# refresh the vendor files and install scraper
go mod vendor
go install -v bitbucket.org/wikivr/api-scraper/cmd/scraper

# run the binary
/go/bin/scraper
