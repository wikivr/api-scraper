package pkg

type Config struct {
	ScraperAddr string
	ScraperPort int
	ComprehensionAddr string
	ComprehensionPort int
}
