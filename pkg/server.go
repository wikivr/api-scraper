package pkg

import (
	"context"
	"fmt"
	"net"
	"os"

	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"

	comprehensionPb "bitbucket.org/wikivr/api-scraper/pkg/proto/gen"
	"bitbucket.org/wikivr/api-scraper/pkg/scraper/wiki"
)

type Server struct {
	logger logrus.Logger
	config Config
	comprehensionClient comprehensionPb.CommunicationClient
	docScraper *wiki.Wiki
}

func (s Server) Ask(ctx context.Context, req *comprehensionPb.QuestionRequest) (*comprehensionPb.QuestionResponse, error) {
	// forward the request to comprehension
	resp, err := s.comprehensionClient.Ask(ctx, req)
	if err != nil {
		s.logger.WithError(err).Error("failed to get response for Ask operation")
	}

	return resp, err
}

func (s Server) SetDocument(ctx context.Context, req *comprehensionPb.DocumentRequest) (*comprehensionPb.DocumentResponse, error) {

	handleName := req.GetHandle()

	if req.GetContent() == "" {
		s.logger.Infof("No content was set by the client")
		// content was not sent by client, hence download the document from Wikipedia
		document, err := s.docScraper.GetDocument(handleName)
		if err != nil {
			s.logger.WithError(err).Errorf("unable to get document for handle: %s", handleName)
			return nil, err
		}

		s.logger.Infof("Document received: %s", string(document))
		// assign to the request and forward to the comprehension server
		req.Content = string(document)
	} else {
		s.logger.Infof("Content was set by client: '%s'", req.GetContent())
	}


	resp, err := s.comprehensionClient.SetDocument(ctx, req)
	if err != nil || resp.GetCode() != comprehensionPb.UploadStatusCode_Ok {
		s.logger.WithError(err).Errorf("failed to set the document. Resp: %+v", resp)
		return nil, err
	}

	return resp, nil
}

func Start(config Config) error {
	logger := logrus.Logger{Out: os.Stderr, Level: logrus.InfoLevel}
	logger.SetFormatter(&logrus.JSONFormatter{})

	s := Server{
		logger: logger,
		config: config,
	}
	err := s.setupGRPCClient()
	if err != nil {
		s.logger.WithError(err).Errorf("failed to setup grpc client")
		return err
	}

	s.docScraper, err = wiki.NewWiki(logger)
	if err != nil {
		s.logger.WithError(err).Errorf("unable to get scraper object")
		return err
	}

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", s.config.ScraperPort))
	if err != nil {
		s.logger.WithError(err).Errorf("failed to setup scraper server on Port: %d", s.config.ScraperPort)
		return err
	}
	grpcServer := grpc.NewServer()
	comprehensionPb.RegisterCommunicationServer(grpcServer, s)

	if err := grpcServer.Serve(lis); err != nil {
		s.logger.WithError(err).Error("failed to start scraper server")
	}

	return nil
}

func (s *Server) setupGRPCClient() error {

	comprehensionEndpoint := fmt.Sprintf("%s:%d", s.config.ComprehensionAddr, s.config.ComprehensionPort)
	conn, err := grpc.Dial(comprehensionEndpoint, grpc.WithInsecure())
	if err != nil {
		s.logger.WithError(err).Errorf("failed to connect to server")
		return err
	}
	s.comprehensionClient = comprehensionPb.NewCommunicationClient(conn)

	return nil
}
