package wiki

import "encoding/json"

type QueryResponse struct {
	BatchComplete bool `json:"batchcomplete"`
	Query WikiQuery `json:"query"` 
}

type WikiQuery struct {
	Pages []WikiPage `json:"pages"` 
}

type WikiPage struct {
	Title string `json:"title"`
	Missing bool `json:"missing"`
	PageID int64 `json:"pageid"`
}

func (q *QueryResponse)Unmarshal(data []byte) error {
	return json.Unmarshal(data, q)
}