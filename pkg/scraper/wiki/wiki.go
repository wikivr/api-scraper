package wiki

import (
	"cgt.name/pkg/go-mwclient/params"
	"fmt"
	"github.com/sirupsen/logrus"

	mwclient "cgt.name/pkg/go-mwclient"
)

type Wiki struct {
	logger logrus.Logger
	wikiclient *mwclient.Client
}

func NewWiki(logger logrus.Logger) (*Wiki, error) {
	w, err := mwclient.New("https://en.wikipedia.org/w/api.php", "prasoon-telang-learn")
	if err != nil {
		logger.WithError(err).Errorf("failed to get media wiki client object")
		return nil, err
	}

	return &Wiki{
		logger: logger,
		wikiclient: w,
	}, nil
}

func (w *Wiki) GetDocument(handleName string) ([]byte, error) {
	// perform query so we can get a more appropriate wikipedia page information
	var queryResp QueryResponse

	queryParam := params.Values{
		"titles": handleName,
		"action": "query",
		"format": "json",
	}

	resp, err := w.wikiclient.GetRaw(queryParam)
	if err != nil {
		w.logger.WithError(err).Errorf("unable to get page data for handle: %s", handleName)
		return nil, err
	}

	err = queryResp.Unmarshal(resp)
	if err != nil {
		w.logger.WithError(err).Errorf("unable to unmarshal to query response for handle: %s", handleName)
		return nil, err
	}

	pages := queryResp.Query.Pages
	if len(pages) < 1 {
		w.logger.Errorf("no page found. Query Response: %+v", queryResp)
		return nil, fmt.Errorf("no page found")
	}
	// TODO (prasoontelang) - get a way around to not only ask for first page
	page := pages[0]

	// now we perform text scraping from the wikipedia page we received
	parseParam := params.Values{
		"action": "parse",
		"prop": "text",
		"format": "json",
	}

	if page.Missing {
		parseParam.Add("page", page.Title)
	} else {
		parseParam.Add("pageid", fmt.Sprintf("%d", page.PageID))
	}

	w.logger.Infof("Parse param: %+v", parseParam)
	resp, err = w.wikiclient.GetRaw(parseParam)
	if err != nil {
		w.logger.WithError(err).Errorf("Unable to parse wiki page for param: %+v", parseParam)
		return nil, err
	}

	return resp, nil
}
