package scraper

type Scraper interface {
	GetDocument(handleName string) ([]byte, error)
}