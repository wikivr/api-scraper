# Registry for building images
REGISTRY ?= wikivr
VERSION ?= latest
SCRAPER_IMAGE ?= $(REGISTRY)/scraper

SCRAPER_TAG ?= "$(SCRAPER_IMAGE):$(VERSION)"

all: build

build: build_scraper

build_scraper:
	docker build --build-arg "VERSION=$(VERSION)" -t $(SCRAPER_TAG) -f ./build/Dockerfile .

run:
	docker run -it --rm --name wikivr_scraper -v "$$PWD:/go/src/bitbucket.org/wikivr/api-scraper" "$$(docker build -f build/Dockerfile.run --quiet .)"

build_cross_platform:
	env GOOS=linux GOARCH=amd64 GOARM=7 go build bitbucket.org/wikivr/api-scraper/cmd/scraper/

push:
	docker push $(SCRAPER_TAG)
